import './App.css';
import NavBarCustom from "./common/NavBar";

import {useEffect} from "react";

import Web3 from "web3";


function App() {
    useEffect(() => {
        loadWeb3()
    }, [])

    const loadWeb3 = async () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider);
        } else {
            window.alert("Non-ethereum browser detected. You should consider trying MetaMask!")
        }
    }

    return (
        <NavBarCustom/>
    );
}

export default App;
