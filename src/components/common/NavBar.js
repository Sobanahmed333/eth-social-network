import Identicon from 'react-identicons';

// import * from '@material-ui/pickers/nav'

function NavBarCustom(props) {
    const {user} = props;

    // let disabled = false;

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand" href="src/components/common/NavBar#">Navbar</a>

                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item active">
                            <a className="nav-link" href="src/components/common/NavBar#">Home <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="src/components/common/NavBar#">Link</a>
                        </li>
                        {/*<li className="nav-item">*/}
                            {/*<a className={`nav-link ${disabled? "disabled": ""}`} href="#">Disabled</a>*/}
                        {/*</li>*/}
                    </ul>
                </div>
            </nav>
            {user?
                <Identicon string={user.username} /> :
                <span></span>
            }
        </>
    )
}

export default NavBarCustom;