pragma solidity ^0.5.0;

contract SocialNetwork {

    // 'D be the id of every post with the increment of 1
    uint postCount = 1;

    // Structs
    struct Post {
        uint id;
        string content;
        uint tipAmount;
        address payable author;
    }

    // Events
    event PostCreated(
        uint id,
        string content,
        uint tipAmount,
        address payable author
    );

    // Events
    event PostTipped(
        uint id,
        string content,
        uint tipAmount,
        address payable author
    );

    mapping(uint => Post) public posts;

    function createPost(string memory _content) public {
        // validating content
        require(bytes(_content).length > 0);
        // creating post
        posts[postCount] = Post(postCount, _content, 0, msg.sender);
        // triggering the event on post creation
        emit PostCreated(postCount, _content, 0, msg.sender);
        // updating count because multiple posts can't have the same ID
        postCount++;
    }

    function tipAuthor(uint _id) public payable {
        // retrieving the post
        Post memory _post = posts[_id];
        // retrieving author from the post
        address payable _author = _post.author;
        // updating author's balance
        _author.transfer(msg.value);
        // updating retrieved post balance
        _post.tipAmount += msg.value;
        // replacing old post object with updated one
        posts[_id] = _post;
        // triggering the event when post is tipped
        emit PostTipped(_post.id, _post.content, _post.tipAmount, _post.author);
    }

}