const SocialNetwork = artifacts.require('./SocialNetwork.sol')

require('chai')
    .use(require('chai-as-promised'))
    .should()

contract('SocialNetwork', ([deployer, author, tipper]) => {
    let socialNetwork;
    const postData = {
        id: 1,
        content: 'test post',
        author: author,
        tipAmount: 0
    }

    before(async () => {
        socialNetwork = await SocialNetwork.deployed()
    })

    describe('deployment', () => {
        it('deploys successfully', async () => {
            const address = SocialNetwork.address
            assert.notEqual(address, 0x0)
            assert.notEqual(address, '')
            assert.notEqual(address, null)
            assert.notEqual(address, undefined)
        })
        it('creating posts', async () => {
            await socialNetwork.createPost(postData.content, {from: author})
            const post = await socialNetwork.posts.call(postData.id)
            assert.equal(post.id, postData.id)
            assert.equal(post.content, postData.content)
            assert.equal(post.author, postData.author)
            assert.equal(post.tipAmount, postData.tipAmount)

            // FAILURE: Post must have content
            await socialNetwork.createPost('', {from: author}).should.be.rejected
        })
        it('tipping the post/author', async () => {
            const result = await socialNetwork.tipAuthor(postData.id, {from: tipper, value: web3.utils.toWei('1', 'Ether')})
            const event = result.logs[0].args
            assert.equal(event.id, postData.id, 'ID is correct')
            assert.equal(event.author, author, 'Author is correct')

            const post = await socialNetwork.posts.call(1)
            assert.equal(event.tipAmount, web3.utils.toWei('1', 'Ether'), 'Post tipAmount is updated')
        })
    })
})
